
export type DayData = { day: string; date: string };

export type XAxis = {
  data: number[];
  id: string;
  dayData?: Array<DayData>;
};

export type YAxis = {
  color: string;
  name: string;
  isVisible: boolean;
} & XAxis;

export type ChartData = {
  x: XAxis;
  y: YAxis[];
};

export type RawData = {
  columns: (number | string)[][];
  types: { [key: string]: 'line' | 'x' };
  names: { [key: string]: string };
  colors: { [key: string]: string };
};

export const processData = (data: RawData): ChartData => {
  const result: ChartData = <ChartData>{};
  for (let i = 0, l = data.columns.length; i < l; ++i) {
    const { columns, types, names, colors } = data;
    let id = <string>columns[i][0];
    let type = types[id];
    if (type === 'x') {
      const xData = <number[]>columns[i].slice(1);
      const reg = /(\w+)\s(\w+\s\d+)/;
      const dayData = xData.map(timestamp => {
        const match = new Date(timestamp).toDateString().match(reg);
        return { day: match![1] + ',', date: match![2] };
      });
      result.x = {
        id,
        data: xData,
        dayData
      };
    } else if (type === 'line') {
      if (!result.y) {
        result.y = [];
      }
      result.y.push({
        id,
        data: <number[]>columns[i].slice(1),
        name: names[id],
        color: colors[id],
        isVisible: true
      });
    }
  }

  return result;
};
