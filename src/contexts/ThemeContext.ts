import {createContext} from 'react';

export const dayMode = {
  background: 'white',
  axisColor: '#c3c3c3',
  textColor: 'black',
  seekerColor: 'rgba(198, 237, 255, 0.75)',
  seekerFillColor: 'rgba(198, 237, 255, 0.2)',
}

export const nightMode: typeof dayMode = {
  background: '#242f3f',
  axisColor: '#838383',
  textColor: 'white',
  seekerColor: 'rgba(74, 90, 97, 0.75)',
  seekerFillColor: 'rgba(74, 90, 97, 0.2)',
}

export default createContext(dayMode);
