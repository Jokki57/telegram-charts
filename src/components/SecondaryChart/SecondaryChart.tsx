import React, { useRef, useState, useEffect, useCallback } from 'react';
import Plot from '../Plot/Plot';
import { XAxis, YAxis } from '../../utils/dataProcessor';
import Seeker from '../Seeker/Seeker';
import './SecondaryChart.scss';

interface IPropTypes {
  xData: XAxis;
  yData: YAxis[];
  onChange: (start: number, scale:number, end: number) => void;
}

const SecondaryChart: React.FunctionComponent<IPropTypes> = ({
  xData,
  yData,
  onChange
}) => {
  const [dims, setDims] = useState<number[] | null>(null);
  const [seekerWidth, setSeekerWidth] = useState(0);
  const [seekerX, setSeekerX] = useState(0);

  useEffect(() => {
    if (dims) {
      const width = dims[0];
      const start = seekerX / width;
      const scale = seekerWidth / width;
      const end = (seekerX + seekerWidth) / width;
      onChange(start, scale, end);
    }
  });

  const onSeekerChange = useCallback((width: number, x: number) => {
    setSeekerWidth(width);
    setSeekerX(x);
  }, [setSeekerWidth, setSeekerX]);

  return (
    <div
      ref={ref => {
        if (ref) {
          const newDims = [ref.offsetWidth, ref.offsetHeight];
          if (!dims) {
            setDims(newDims);
          }
        }
      }}
      className="SecondaryChart"
    >
      {dims && (
        <Plot
          x={xData}
          ys={yData}
          width={dims[0]}
          height={dims[1]}
          maxCoeficient={1.2}
          renderAxises={false}
        />
      )}
      {dims && (
        <Seeker
          rootWidth={dims[0]}
          onChange={onSeekerChange}
        />
      )}
    </div>
  );
};

export default React.memo(SecondaryChart);
