import React, { useMemo, useContext } from 'react';
import Text from '../Svgs/Text';
import { DayData } from '../../utils/dataProcessor';
import Theme from '../../contexts/ThemeContext';

const LABEL_COUNT = 6;

interface IPropTypes {
  width: number;
  height: number;
  start: number;
  end: number;
  dayData: Array<DayData>;
}

const XLabelsContainer: React.FunctionComponent<IPropTypes> = ({ width, height, dayData, start, end }) => {
  const theme = useContext(Theme);
  
  return (
    <React.Fragment>
      {renderLabels(width, height, dayData, start, end, theme.axisColor)}
    </React.Fragment>
  );
};

function renderLabels(
  width: number,
  height: number,
  dayData: Array<DayData>,
  start: number,
  end: number,
  color: string,
) {
  const length = dayData.length - 1;
  const startIndex = Math.floor(length * start);
  const endIndex = Math.ceil(length * end);
  const newArray = dayData.slice(startIndex, endIndex + 1);
  const dL = Math.floor(newArray.length / LABEL_COUNT);

  const result = [];
  for (let i = 0, l = LABEL_COUNT; i < l; ++i) {
    result.push(
      <Text
        absHeight={height}
        absWidth={width}
        key={i}
        color={color}
        perX={i / LABEL_COUNT}
        perY={0}
        yOffset={0}
        text={newArray[i * dL].date}
      />
    );
  }
  return result;
}

export default React.memo(XLabelsContainer);
