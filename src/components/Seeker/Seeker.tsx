import React, { useState, useEffect, useRef, useContext } from 'react';
import Theme from '../../contexts/ThemeContext';
import './Seeker.scss';

interface IPropTypes {
  onChange: (width: number, x: number) => void;
  rootWidth: number;
}

const BORDER_WIDTH = 23;
const MIN_WIDTH = 0.01;
type MouseDownType = 'left' | 'right' | 'center';
interface IEventOffset {
  type: MouseDownType;
  startPoint: number;
  startOffset?: number;
}

const Seeker: React.FunctionComponent<IPropTypes> = ({
  onChange,
  rootWidth
}) => {
  const [width, setWidth] = useState(rootWidth);
  const [mouseEvent, setMouseEvent] = useState<IEventOffset | null>(null);
  const [x, setX] = useState(0);
  const xRef = useRef(x);

  useEffect(() => {
    onChange(width, x);
  }, [width, x]);

  useEffect(() => {
    xRef.current = x;
  }, [x]);

  const theme = useContext(Theme);

  const onMouseDown = (event: React.MouseEvent | React.TouchEvent) => {
    const clientX =
      event.type === 'mousedown'
        ? (event as React.MouseEvent).clientX
        : (event as React.TouchEvent).touches[0].clientX;
    const offsetX = getOffsetX(clientX, event.target as HTMLElement);
    if (offsetX < 0) {
      setMouseEvent({
        type: 'left',
        startPoint: clientX,
        startOffset: offsetX
      });
    } else if (offsetX > width - BORDER_WIDTH * 2) {
      setMouseEvent({ type: 'right', startPoint: clientX });
    } else {
      setMouseEvent({ type: 'center', startPoint: clientX });
    }
  };

  useEffect(() => {
    function onMouseMove(event: MouseEvent | TouchEvent) {
      if (mouseEvent) {
        const clientX =
          event.type === 'mousemove'
            ? (event as MouseEvent).clientX
            : (event as TouchEvent).touches[0].clientX;
        const { type, startPoint, startOffset } = mouseEvent;
        if (type === 'left') {
          let newWidth = width + startPoint - clientX;
          let newX = clientX + startOffset!;
          if (newWidth + clientX > rootWidth) {
            newX = rootWidth - newWidth;
          }
          if (newX < 0) {
            newWidth += newX;
            newX = 0;
          } else if (newWidth < rootWidth * MIN_WIDTH + BORDER_WIDTH * 2) {
            newWidth = rootWidth * MIN_WIDTH + BORDER_WIDTH * 2;
            newX = xRef.current;
          }

          setWidth(newWidth);
          setX(newX);
        } else if (type === 'right') {
          let newWidth = width + clientX - startPoint;
          if (newWidth < rootWidth * MIN_WIDTH + BORDER_WIDTH * 2) {
            newWidth = rootWidth * MIN_WIDTH + BORDER_WIDTH * 2;
          } else if (x + newWidth > rootWidth) {
            newWidth = rootWidth - x;
          }
          setWidth(newWidth);
        } else if (type === 'center') {
          let newX = x + clientX - startPoint;
          if (newX < 0) {
            newX = 0;
          } else if (newX + width > rootWidth) {
            newX = rootWidth - width;
          }
          setX(newX);
        }
      }
    }

    function onMouseUp() {
      window.removeEventListener('mousemove', onMouseMove);
      window.removeEventListener('mouseup', onMouseUp);
      window.removeEventListener('touchmove', onMouseMove);
      window.removeEventListener('touchend', onMouseUp);
    }

    if (mouseEvent) {
      window.addEventListener('mousemove', onMouseMove);
      window.addEventListener('mouseup', onMouseUp);
      window.addEventListener('touchmove', onMouseMove);
      window.addEventListener('touchend', onMouseUp);
    }
    return () => {
      window.removeEventListener('mousemove', onMouseMove);
      window.removeEventListener('mouseup', onMouseUp);
      window.removeEventListener('touchmove', onMouseMove);
      window.removeEventListener('touchend', onMouseUp);
    };
  }, [mouseEvent]);

  return (
    <div className="Seeker">
      <div
        className="Seeker__Left"
        style={{
          width: `${x}px`,
          background: theme.seekerFillColor,
        }}
      />
      <div
        className="Seeker__Center"
        style={{
          transform: `translateX(${x}px)`,
          width: `${width}px`,
          borderRight: getBroderStyle(theme.seekerColor, false),
          borderLeft: getBroderStyle(theme.seekerColor, false),
          borderTop: getBroderStyle(theme.seekerColor, true),
          borderBottom: getBroderStyle(theme.seekerColor, true),
        }}
        onMouseDown={onMouseDown}
        onTouchStart={onMouseDown}
      />
      <div
        className="Seeker__Right"
        style={{
          width: `${Math.round(rootWidth - (x + width))}px`,
          transform: `translateX(${x + width}px)`,
          background: theme.seekerFillColor,
        }}
      />
    </div>
  );
};

function getOffsetX(clientX: number, target: HTMLElement) {
  return clientX - target.getBoundingClientRect().left - BORDER_WIDTH;
}

function getBroderStyle(color: string, isVertical: boolean) {
  return `solid ${color} ${isVertical ? '3px' : `${BORDER_WIDTH}px`}`;
}

export default React.memo(Seeker);
