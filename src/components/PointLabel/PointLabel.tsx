import React, { useRef, useContext, useState, useMemo } from 'react';
import { DayData } from '../../utils/dataProcessor';
import ThemeContext from '../../contexts/ThemeContext';
import './PointLabel.scss';

interface IPropTypes {
  values: IValue[];
  xLabel: DayData;
  x: number;
}

interface IValue {
  color: string;
  value: number;
  label: string;
}

const X_NEGATIVE_OFFSET = 15;

const PointLabel: React.FunctionComponent<IPropTypes> = ({
  values,
  x,
  xLabel
}) => {
  const [lineHeight, setLineHeight] = useState(0);
  const [parentWidth, setParentWidth] = useState(0);
  const rootRef = useRef<HTMLDivElement | null>(null);
  const theme = useContext(ThemeContext);
  const offsetX = useMemo(() => {
    let result = X_NEGATIVE_OFFSET;
    if (rootRef.current) {
      const { current: root } = rootRef;
      if (root.offsetWidth + x - X_NEGATIVE_OFFSET > parentWidth - 20) {
        result = root.offsetWidth + x - X_NEGATIVE_OFFSET - parentWidth + 20;
      }
    }
    return result;
  }, [parentWidth, x]);

  return (
    <div
      ref={ref => {
        if (ref) {
          const parentHeight = ref.parentElement!.offsetHeight;
          setLineHeight(parentHeight - ref.offsetHeight - ref.offsetTop + 7);
          setParentWidth(ref.parentElement!.offsetWidth);
        }
        rootRef.current = ref;
      }}
      className="PointLabel"
      style={{ transform: `translateX(${x}px)` }}
    >
      <div
        className="PointLabel__LabelWrapper"
        style={{
          transform: `translateX(-${offsetX}px)`,
          background: theme.background,
          maxWidth: parentWidth
        }}
      >
        <div
          className="PointLabel__XLabel"
          style={{ color: theme.textColor }}
        >{`${xLabel.day} ${xLabel.date}`}</div>
        <div className="PointLabel__ValueWrapper">
          {values.map((v, i) => (
            <div key={i} style={{ color: v.color }}>
              <div className="PointLabel__Value">{v.value}</div>
              <div className="PointLabel__Label">{v.label}</div>
            </div>
          ))}
        </div>
      </div>
      <div className="PointLabel__Line" style={{ height: `${lineHeight}px` }} />
    </div>
  );
};

export default React.memo(PointLabel);
