import React, { useState, useMemo, useContext } from 'react';
import { XAxis, YAxis } from '../../utils/dataProcessor';
import { Svg, HorLine, Polyline, Text } from '../Svgs';
import { IDotsInfo } from '../Svgs/Polyline';
import XLabelsContaier from '../XLabelsContainer/XLablesContainer';
import PointLabel from '../PointLabel/PointLabel';
import Theme from '../../contexts/ThemeContext';
import './Plot.scss';

const flat = require('array.prototype.flat');

const AXIS_COUNT = 6;

type IPropTypes = {
  x: XAxis;
  ys: YAxis[];
  width: number;
  height: number;
  scaleWidth?: number;
  perOffsetX?: number;
  perOffsetEndX?: number;
} & Partial<IDefaultProps>;

interface IDefaultProps {
  maxCoeficient: number;
  renderAxises: boolean;
}

const Plot: React.FunctionComponent<IPropTypes> = ({
  x,
  ys,
  width,
  height,
  maxCoeficient,
  renderAxises,
  perOffsetX,
  scaleWidth,
  perOffsetEndX
}) => {
  const allValues = useMemo(
    () => [...ys.filter(y => y.isVisible).map(y => y.data)],
    [ys]
  );

  const max = useMemo(
    () =>
      getMaxMinWithBounds(
        allValues,
        maxCoeficient,
        true,
        perOffsetX,
        perOffsetEndX
      ),
    [allValues, maxCoeficient, perOffsetX, perOffsetEndX]
  );

  const min = useMemo(
    () =>
      getMaxMinWithBounds(
        allValues,
        maxCoeficient,
        false,
        perOffsetX,
        perOffsetEndX
      ),
    [allValues, maxCoeficient, perOffsetX, perOffsetEndX]
  );

  const plotHeight = height - 20;
  let relLinesPlotHegiht = plotHeight;
  const dY = max - min;
  let numbersMin = 0;
  if (dY < min) {
    const relHeightInc = (min * 0.95) / max;
    relLinesPlotHegiht /= relHeightInc;
    numbersMin = min / 0.95;
  }

  const [mouseX, setMouseX] = useState(-1);
  const [pointLabelInfo, setPointLabelInfo] = useState<IDotsInfo | null>(null);
  const onMouseMove = (event: React.MouseEvent | React.TouchEvent) => {
    const x =
      event.type === 'mousemove'
        ? (event as React.MouseEvent).clientX
        : (event as React.TouchEvent).touches[0].clientX;
    setMouseX(x);
  };

  const theme = useContext(Theme);

  const onMouseLeave = () => setMouseX(-1);

  const scaledWidth = scaleWidth ? width / scaleWidth : width;
  const offsetX = perOffsetX ? Math.round(perOffsetX * scaledWidth) : 0;

  return (
    <div
      onMouseMove={onMouseMove}
      onTouchMove={onMouseMove}
      onMouseUp={onMouseLeave}
      onMouseLeave={onMouseLeave}
      onTouchEnd={onMouseLeave}
    >
      {mouseX !== -1 && pointLabelInfo && (
        <PointLabel
          x={mouseX}
          xLabel={x.dayData![pointLabelInfo.dotIndex]}
          values={ys
            .filter(y => y.isVisible)
            .map(y => ({
              color: y.color,
              label: y.name,
              value: y.data[pointLabelInfo.dotIndex]
            }))}
        />
      )}
      <Svg width={width} height={height}>
        {renderAxises && renderAxies(width, plotHeight, theme.axisColor)}
        {renderAxises &&
          perOffsetX !== undefined &&
          perOffsetEndX !== undefined && (
            <XLabelsContaier
              width={width}
              height={height} // this is crucial, x labels need full height
              dayData={x.dayData!}
              start={perOffsetX}
              end={perOffsetEndX}
            />
          )}
        {renderPolylines(
          width,
          relLinesPlotHegiht,
          max,
          ys,
          offsetX,
          scaleWidth,
          mouseX,
          setPointLabelInfo
        )}
        {renderAxises &&
          renderNumbers(width, plotHeight, max, numbersMin, theme.axisColor)}
      </Svg>
    </div>
  );
};

(Plot.defaultProps as IDefaultProps) = {
  maxCoeficient: 1.05,
  renderAxises: true
};

function renderAxies(
  width: number,
  height: number,
  color: string
): React.ReactNodeArray | any[] {
  const result: any[] = [];

  for (let i = 0; i < AXIS_COUNT; ++i) {
    const y = i / AXIS_COUNT;
    result.push(
      <HorLine
        absHeight={height}
        absWidth={width}
        key={i}
        strokeColor={color}
        strokeWidth={1}
        perY={y}
      />
    );
  }
  return result;
}

function renderPolylines(
  width: number,
  height: number,
  max: number,
  ys: YAxis[],
  offsetX: number = 0,
  scaleWidth: number = 1,
  mouseX: number,
  setPointsInfo: (value: any) => void
): React.ReactNodeArray {
  const result = [];

  for (let i = 0, l = ys.length; i < l; ++i) {
    result.push(
      <Polyline
        absHeight={height}
        absWidth={width}
        key={ys[i].id}
        strokeColor={ys[i].color || ''}
        points={ys[i].data}
        max={max}
        isVisible={ys[i].isVisible}
        offsetX={offsetX}
        scaleWidth={scaleWidth}
        mouseX={mouseX}
        setHoveredDot={setPointsInfo}
      />
    );
  }
  return result;
}

function renderNumbers(
  width: number,
  height: number,
  max: number,
  min: number,
  color: string
): React.ReactNodeArray {
  const result = [];

  const dY = (max - min) / AXIS_COUNT;
  for (let i = 0; i < AXIS_COUNT; ++i) {
    const y = i / AXIS_COUNT;
    let text;
    if (max === 0 && i > 0) {
      text = '';
    } else {
      text = Math.round((dY * i + min) * 100) / 100;
    }
    result.push(
      <Text
        absHeight={height}
        absWidth={width}
        key={i}
        color={color}
        perX={0}
        perY={y}
        yOffset={-10}
        text={text}
      />
    );
  }
  return result;
}

function getMaxMin(
  arr: number[],
  maxCoeficient: number = 1,
  isMax: boolean = true
) {
  return (isMax ? Math.max : Math.min).apply(Math, arr) * maxCoeficient;
}

function getMaxMinWithBounds(
  arr: number[][],
  maxCoeficient: number = 1,
  isMax: boolean,
  start?: number,
  end?: number
) {
  if (arr.length === 0) {
    return 0;
  }
  if (start !== undefined && end !== undefined) {
    const maxEachValue = arr.map(a => {
      const length = a.length - 1;
      const startIndex = Math.floor(length * start);
      const endIndex = Math.ceil(length * end);
      const newArray = a.slice(startIndex, endIndex + 1);
      return getMaxMin(newArray, maxCoeficient, isMax);
    });
    const max = getMaxMin(maxEachValue, maxCoeficient, isMax);

    return max;
  } else {
    // const a = arr.flat();
    const a = flat(arr, 1);
    return getMaxMin(a, maxCoeficient, isMax);
  }
}

export default React.memo(Plot);
