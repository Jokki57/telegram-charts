import React, { useContext, useRef } from 'react';
import Theme from '../../contexts/ThemeContext';
import './LineSelector.scss';

export type ILines = {
  [key: string]: {
    checked: boolean;
    color: string;
  };
};

interface IPropTypes {
  lines: ILines;
  onChange: (line: string, state: boolean) => void;
}

const LineSelector: React.FunctionComponent<IPropTypes> = ({
  lines,
  onChange
}) => {
  const theme = useContext(Theme);
  const checkboxRef = useRef(null);

  return (
    <div className="LineSelector">
      {Object.entries(lines).map(([name, lineInfo]) => (
        <div
          className="LineSelector__Wrapper"
          key={name}
          style={{ borderColor: theme.axisColor }}
        >
          <div
            className="LineSelector__Container"
            style={{ color: theme.textColor }}
            // onClick={e => {
            //   if (checkboxRef.current) {
            //   onChange(name, checkboxRef.current.checked)
            //   }
            // }}
          >
            {name}
            <input
              ref={checkboxRef}
              type="checkbox"
              checked={lineInfo.checked}
              onChange={e => onChange(name, e.currentTarget.checked)}
            />
            <span
              className={`LineSelector__Checkmark ${
                !lineInfo.checked ? 'LineSelector__Checkmark_Unchecked' : ''
              }`}
              style={{
                background: lineInfo.checked ? lineInfo.color : 'transparent',
                [!lineInfo.checked ? 'border' : '']: `solid 2px ${lineInfo.color}`,
              }}
            />
          </div>
        </div>
      ))}
    </div>
  );
};

export default React.memo(LineSelector);

{
  /* <FormControlLabel
          key={name}
          control={
            <Checkbox
              checked={checked}
              onChange={(e, checked) => onChange(name, checked)}
            />
          }
          label={name}
        /> */
}
