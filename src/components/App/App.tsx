import React, { useState, useMemo, useCallback } from 'react';
import MainChart from '../MainChart/MainChart';
import SecondaryChart from '../SecondaryChart/SecondaryChart';
import LineSelector, { ILines } from '../LineSelector/LineSelector';
import './App.scss';
import chart_data from '../../chart_data.json';
import Theme, { dayMode, nightMode } from '../../contexts/ThemeContext';
import DatasetSelector from '../DatasetSelector/DatasetSelector';
import {
  ChartData,
  RawData,
  processData,
  YAxis
} from '../../utils/dataProcessor';

const chartData: ChartData[] = [];
chart_data.forEach(d => {
  chartData.push(processData(d as RawData));
});

const App: React.FunctionComponent = () => {
  const [data, setData] = useState(chartData[0]);
  const defaultLines = useMemo(() => getDefaultLinesState(data), []);

  const [lines, setLines] = useState<ILines>(defaultLines);
  const processedLines = useMemo(() => processLinesVisibilty(data.y, lines), [
    data.y,
    lines
  ]);
  const [bounds, setBounds] = useState([0, 0]);
  const onSecChartChange = useCallback<
    (s: number, scale: number, e: number) => void
  >((start: number, scale: number, end: number) => {
    setBounds([start, scale, end]);
  }, []);

  const onDatasetChange = useCallback(index => {
    setData(chartData[index]);
    setLines(getDefaultLinesState(chartData[index]));
  }, []);

  const [isDayMode, setIsDayMode] = useState(true);

  return (
    <Theme.Provider value={isDayMode ? dayMode : nightMode}>
      <div
        className="App"
        style={{
          background: isDayMode ? dayMode.background : nightMode.background
        }}
      >
        <DatasetSelector
          data={chartData.map((_, i) => i.toString())}
          onChange={onDatasetChange}
        />
        <MainChart xData={data.x} yData={processedLines} bounds={bounds} />
        <SecondaryChart xData={data.x} yData={processedLines} onChange={onSecChartChange} />
        <LineSelector
          lines={lines}
          onChange={(name, checked) => {
            const newLines = { ...lines };
            newLines[name] = { checked: checked, color: lines[name].color };
            setLines({ ...newLines });
          }}
        />
        <div
          className="App__Button"
          onClick={() => {
            setIsDayMode(!isDayMode);
          }}
        >
          {`Switch to ${isDayMode ? 'night' : 'day'} mode`}
        </div>
      </div>
    </Theme.Provider>
  );
};

function processLinesVisibilty(data: YAxis[], linesState: ILines) {
  return data.map(y => ({ ...y, isVisible: linesState[y.name].checked }));
}

function getDefaultLinesState(data: ChartData) {
  const result: ILines = {};
  data.y.forEach(
    line => (result[line.name] = { checked: true, color: line.color })
  );
  return result;
}

export default App;
