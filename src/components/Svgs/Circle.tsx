import React from 'react';
import './styles.scss';

interface IPropTypes {
  y: number;
  x: number;
}

const Circle: React.FunctionComponent<IPropTypes> = ({
  y,
  x,
}) => {

  return (
    <circle
      cx={x}
      cy={y}
      r="5"
      vectorEffect="non-scaling-stroke"
    />
  );
};

export default React.memo(Circle);
