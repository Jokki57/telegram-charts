import React from 'react';
import './styles.scss';

interface IPropTypes {
  children: React.ReactNode;
  width: number,
  height: number,
}

const Svg = React.forwardRef<SVGElement, IPropTypes>(({ children, width, height }, ref) => {
  return <svg width={width} height={height} ref={ref as React.RefObject<SVGSVGElement>}>{children}</svg>;
});

export default React.memo(Svg);
