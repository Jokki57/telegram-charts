import React from 'react';
import './styles.scss'

interface IPropTypes {
  absWidth: number,
  absHeight: number,
  perY: number,
  perX: number
  color: string,
  text: string | number,
  yOffset: number
}

const Text: React.FunctionComponent<IPropTypes>= ({absWidth, absHeight, perY, perX, color, text, yOffset}) => {

  return (
    <text className="AxisText" fill={color} x={absWidth * perX} y={(1 - perY) * absHeight + yOffset}>{text}</text>
  );
};

export default React.memo(Text);
