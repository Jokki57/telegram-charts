import React, { useMemo, useRef, useLayoutEffect, useEffect } from 'react';
import Circle from './Circle';
import './styles.scss';

type IPropTypes = {
  points: number[];
  absWidth: number;
  absHeight: number;
  max: number;
  strokeColor: string;
  offsetX: number;
  scaleWidth: number;
  mouseX: number;
  setHoveredDot: (info: IDotsInfo | null) => void;
} & Partial<IDefaultProps>;

interface IDefaultProps {
  isVisible: boolean;
}

export interface IDotsInfo {
  dotIndex: number;
  coords: number[];
}

const Polyline: React.FunctionComponent<IPropTypes> = ({
  points,
  max,
  absHeight,
  absWidth,
  strokeColor,
  isVisible,
  offsetX,
  scaleWidth,
  mouseX,
  setHoveredDot
}) => {
  const prevPointsString = useRef<string | undefined>(undefined);
  const prevPoints = useRef<number[] | undefined>(undefined);
  const prevMax = useRef<number | undefined>(undefined);
  const animateRef = useRef<any>(null);

  const [dotsPoints, pointsString] = useMemo(() => {
    if (!prevPointsString.current) {
      [, prevPointsString.current] = getPoints(absWidth, absHeight, 0, points);
    } else {
      [, prevPointsString.current] = getPoints(
        absWidth,
        absHeight,
        prevMax.current,
        prevPoints.current
      );
    }
    prevPoints.current = points;
    prevMax.current = max;
    return getPoints(absWidth, absHeight, isVisible ? max : 0, points);
  }, [max, absWidth, absHeight, points, isVisible]);

  useLayoutEffect(() => {
    if (animateRef.current) {
      animateRef.current.beginElement();
    }
  }, [pointsString, isVisible]);

  useEffect(() => {
    if (!isVisible) {
      prevPoints.current = undefined;
      prevPointsString.current = undefined;
    }
  }, [isVisible]);

  // const dotIndex = 0;
  const dotIndex =
    mouseX >= 0
      ? Math.min(
          Math.round(
            ((mouseX + offsetX) / (absWidth / scaleWidth)) * dotsPoints.length
          ),
          dotsPoints.length - 1
        )
      : -1;
  useEffect(() => {
    setHoveredDot(
      dotIndex >= 0 ? { dotIndex, coords: dotsPoints[dotIndex] } : null
    );
  }, [dotIndex]);

  return (
    <g
      fill="transparent"
      className={`Polyline ${!isVisible ? 'Polyline_Hidden' : ''}`}
    >
      <polyline
        points={pointsString}
        vectorEffect="non-scaling-stroke"
        stroke={strokeColor}
        strokeWidth="3"
        fill="none"
        style={{
          transform: `translateX(-${offsetX}px) scaleX(${1 / scaleWidth})`
        }}
      >
        <animate
          ref={animateRef}
          attributeName="points"
          dur="200ms"
          repeatCount="1"
          from={prevPointsString.current}
          to={pointsString}
        />
      </polyline>
      <g
        strokeWidth="3"
        fill="white"
        stroke={strokeColor}
        style={{
          transform: `translateX(-${offsetX}px)`
        }}
      >
        {scaleWidth !== 0 &&
          dotIndex !== -1 &&
          renderDots(dotsPoints, dotIndex, scaleWidth)}
      </g>
    </g>
  );
};

function getPoints(
  absWidth: number,
  absHeight: number,
  max: number = 0,
  rawPoints: number[] = []
): [number[][], string] {
  const len = rawPoints.length - 1;
  const points = rawPoints.map((point, i) => [
    Math.round((i / len) * absWidth),
    max === 0 ? absHeight : Math.round((1 - point / max) * absHeight)
  ]);
  const pointsString = points.map(p => `${p[0]},${p[1]}`).join(' ');
  return [points, pointsString];
}

function renderDots(points: number[][], index: number, scaleWidth: number) {
  const [x, y] = points[index];
  return <Circle x={x / scaleWidth} y={y} />;
}

(Polyline.defaultProps as IDefaultProps) = {
  isVisible: true
};

export default React.memo(Polyline);
