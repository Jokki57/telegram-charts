import React from 'react';
import './styles.scss';

interface IPropTypes {
  absWidth: number;
  absHeight: number;
  perY: number;
  strokeWidth: number;
  strokeColor: string;
}

const HorLine: React.FunctionComponent<IPropTypes> = ({
  absWidth,
  perY,
  absHeight,
  strokeColor,
  strokeWidth
}) => {
  const y = (1 - perY) * absHeight;

  return (
    <line
      className="HorLine"
      x1="0"
      x2={absWidth}
      y1={y}
      y2={y}
      style={{ stroke: strokeColor, strokeWidth: strokeWidth }}
    />
  );
};

export default React.memo(HorLine);
