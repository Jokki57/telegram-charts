import React, { useState, useMemo } from 'react';
import Plot from '../Plot/Plot';
import { XAxis, YAxis } from '../../utils/dataProcessor';
import './MainChart.scss';

interface IPropTypes {
  xData: XAxis;
  yData: YAxis[];
  bounds: number[];
}

const MainChart: React.FunctionComponent<IPropTypes> = ({
  xData,
  yData,
  bounds
}) => {
  const [dims, setDims] = useState<number[] | null>(null);

  return (
    <div
      ref={ref => {
        if (ref) {
          const newDims = [ref.offsetWidth, ref.offsetHeight];
          if (!dims) {
            setDims(newDims);
          }
        }
      }}
      className="MainChart"
    >
      {dims && (
        <Plot
          x={xData}
          ys={yData}
          width={dims[0]}
          height={dims[1]}
          perOffsetX={bounds[0]}
          scaleWidth={bounds[1]}
          perOffsetEndX={bounds[2]}
        />
      )}
    </div>
  );
};

export default React.memo(MainChart);
