import React, { useContext, useMemo, useEffect } from 'react';
import Theme from '../../contexts/ThemeContext';

interface IPropTypes {
  data: string[];
  onChange: (value: number) => void;
}

const DatasetSelector: React.FunctionComponent<IPropTypes> = ({
  data,
  onChange
}) => {
  const theme = useContext(Theme);
  const defaultDataset = useMemo(() => {
    let result = null;
    const match = window.location.search.match(/dataset=(\d+)/);
    if (match && match[1] && parseInt(match[1]) < data.length) {
      result = match[1];
    }
    return result;
  }, [data]);

  useEffect(() => {
    if (defaultDataset !== null) {
      onChange(parseInt(defaultDataset, 10));
    }
  }, []);

  return (
    <div className="DatasetSelector" style={{ color: theme.textColor }}>
      {'Select dataset: '}
      <select
        defaultValue={defaultDataset || ''}
        onChange={event => {
          onChange(parseInt(event.currentTarget.value));
        }}
      >
        {data.map(d => (
          <option key={d} value={d}>
            {d}
          </option>
        ))}
      </select>
    </div>
  );
};

export default React.memo(DatasetSelector);
